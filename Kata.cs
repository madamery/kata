using System;
using System.Collections.Generic;
namespace CalculatorApplication
{
   class Number
   {
		public string[] getDelimiter(string arg){
			string delimiter = null;
			List<string> delimiters = new List<string>();
			for (int i = 3; i < arg.Length; i++) {
				if (arg [i] != ']' && arg [i] != '[') {
					delimiter += arg [i];

				} else if (arg [i] == ']') {
					delimiters.Add (delimiter);
					delimiter = null;

				}

			}
			return delimiters.ToArray();
		}
		public int start = 0;
      public int add(string numbers) {
		  int sum = 0;
		  string negativeNumbers = null;
		  if (numbers == null) {
		      sum = 0;
		      
		  } else if (numbers.Length == 0) {
			  sum = 0;
			  
		  } else if (numbers.Length == 1) {
			  sum = (int)char.GetNumericValue(numbers[0]);
			  
		  } else {
				string[] numberArr;
				string[] newDelimiter = null;
				if(numbers.IndexOf("//") != -1) {
					numberArr = numbers.Split(new string[] { "," , "\n"}, StringSplitOptions.None);
					newDelimiter = getDelimiter (numberArr [0]);
					start = newDelimiter.Length + 1;
				}
				string[] delimiters = new string[] { ",", "\n" };
				if (newDelimiter != null) {
					List<string> list = new List<string>();
					list.AddRange(delimiters);
					list.AddRange(newDelimiter);
					string[] tempArr = list.ToArray();
					delimiters = tempArr;

				}
				numberArr = numbers.Split(delimiters, StringSplitOptions.None);
			    int tempInt =0;
				for (int i= start; i<numberArr.Length; i++) {
					tempInt = int.Parse(numberArr[i]);
					if (tempInt < 0) {
						negativeNumbers += numberArr [i] + ",";

					} else if (tempInt > 1000) {
						//do nothing

					} else {
						sum += tempInt;

					}
				  
			  	}
			  
		  	}
			if (negativeNumbers != null) {
				throw new Exception("negatives not allowed , negative that was passed: " + negativeNumbers);

			} else {
				return sum;

			}
		  
      }
      static void Main(string[] args) {
         	Number n = new Number();
			//last step already handled in the previous step.
			int sum = n.add("//[***][%][$$]\n1***2%3$$1***10%1");
         	Console.WriteLine("The Sum is : {0}", sum);
         	Console.ReadLine();
		 
      }
   }
}
